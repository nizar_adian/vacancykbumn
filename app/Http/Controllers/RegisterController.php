<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use DB;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->__route = 'auth.register';
    }

    public function index()
    {
        return view($this->__route, [
            'title' => 'Register',
        ]);
    }

    // public function store(Request $request)
    // {
    //     $validatedData = $request->validate([
    //         'name' => 'required',
    //         'email' => 'required|unique:users,email',
    //         'nik' => 'required|unique:users,nik',
    //         'password' => 'required'
    //     ]);

    //     $validatedData['password'] = Hash::make($validatedData['password']);
    //     User::create($validatedData);
    //     return response()->json([
    //         "status" => true,
    //         "msg" => "You have successfully registered, Login to access your dashboard",
    //         "redirect_location" => url("login")
    //     ]);
    // }

    public function store(Request $request)
    {

      $result = [
        'flag'  => 'warning',
        'msg' => 'Error System',
        'title' => 'Gagal proses data'
      ];
      if(strlen($request->nik) != 16 || substr($request->nik, 12, 4) == '0000' ){
        $result['msg'] = '<ul>NIK tidak valid</ul>';
        return response()->json($result);
      }
      if($request->password != $request->confirm_password){
        $result['msg'] = '<ul>Konfirmasi Password Tidak Sama</ul>';
        return response()->json($result);
      }

      $validator = $this->validateform($request);

      if (!$validator->fails()) {
            $param = $request->except(['id', '_token', '_method', 'actionform','confirm_password']);
            DB::beginTransaction();
            try{
                $param['password'] = Hash::make($param['password']);

                $data = User::create($param);
                DB::commit();
                $result = [
                  'flag'  => 'success',
                  'msg' => 'Sukses Register Data',
                  'title' => 'Sukses'
                ];
             }catch(\Exception $e){
                DB::rollback();
                $result = [
                  'flag'  => 'warning',
                  'msg' => $e->getMessage(),
                  'title' => 'Gagal'
                ];
             }

      }else{
          $messages = $validator->errors()->all('<li>:message</li>');
          $result = [
              'flag'  => 'warning',
              'msg' => '<ul>'.implode('', $messages).'</ul>',
              'title' => 'Gagal proses data abc'
          ];
      }

      return response()->json($result);
    }

    protected function validateform($request)
    {
        $required['name'] = 'required';
        $required['nik'] = 'required|unique:users,nik';
        $required['email'] = 'required|unique:users,email';
        $required['password'] = 'required';
        $required['confirm_password'] = 'required';

        $message['name.required'] = 'Nama Lengkap wajib Diisi';
        $message['nik.required'] = 'NIK wajib Diisi';
        $message['email.required'] = 'Email wajib Diisi';
        $message['password.required'] = 'Password wajib Diisi';
        $message['confirm_password.required'] = 'Konfirmasi Password salah';

        return Validator::make($request->all(), $required, $message);
    }

}
