<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->__route = 'auth.login';
    }

    public function index()
    {
        return view($this->__route, [
            'title' => 'Login',
        ]);
    }

    public function authenticate(Request $request) {

        // $credential = $request->validate([
        //     'email' => ['required'],
        //     'password' => ['required'],
        // ]);


        // if (Auth::attempt($credential)) {
        //     $request->session()->regenerate();
        //     return redirect()->intended('/peserta/beranda');
        // }

        // return back()->with('loginError', 'Login Failed');

        $result = [
            'flag'  => 'warning',
            'msg' => 'Error System',
            'title' => 'Gagal proses data'
        ];

        $validator = $this->validateform($request);

        if (!$validator->fails()) {

            $credential = $request->validate([
                    'email' => ['required'],
                    'password' => ['required'],
                ]);
            if (Auth::attempt($credential)) {
                $request->session()->regenerate();
                return $result = [
                    'flag'  => 'error',
                    'msg' => 'Username/Password Tidak Valid',
                    'title' => 'Gagal',
                    'url' => '/peserta/beranda'
                ];
                // return redirect()->intended('/peserta/beranda');
            }else{
                $result = [
                    'flag'  => 'error',
                    'msg' => 'Username/Password Tidak Valid',
                    'title' => 'Gagal'
                ];
            }
        }else{
            $messages = $validator->errors()->all('<li>:message</li>');
            $result = [
                'flag'  => 'error',
                'msg' => '<ul>'.implode('', $messages).'</ul>',
                'title' => 'Gagal proses data'
            ];
        }

        return response()->json($result);
    }

    protected function validateform($request)
    {
        $required['email'] = 'required';
        $required['password'] = 'required';

        $message['email.required'] = 'Email wajib Diisi';
        $message['password.required'] = 'Password wajib Diisi';

        return Validator::make($request->all(), $required, $message);
    }

    public function logout() {
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/');
    }
}
