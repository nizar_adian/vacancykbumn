$(document).ready(function() {

    $('#kt_sign_in_form').validate({
        submitHandler: function(form) {
            var dataform = $('#kt_sign_in_form').serialize();
            $.ajax({
                type: 'POST',
                url: urlsign,
                data: dataform,
                beforeSend: function() {},
                success: function(data) {
                    // Swal.fire({
                    //     title: data.title,
                    //     html: data.msg,
                    //     type: data.flag,
                    //     icon: data.flag,
                    //     showCancelButton: false,
                    //     confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                    //     confirmButtonClass: "btn btn-success"
                    // }).then((result) => {
                    //     if (result.isConfirmed) {
                    //         if (data.flag == 'success') {
                    //             clearform();
                    window.location = data.url;
                    //         }
                    //     }
                    // })
                },
                error: function(e) {
                    console.log(e);
                    swal.fire({
                        title: "Error System",
                        html: e, //'Coba ulangi kembali !!!',
                        type: 'error',
                        icon: 'error',
                        buttonsStyling: false,

                        confirmButtonText: "<i class='flaticon2-checkmark'></i> OK",
                        confirmButtonClass: "btn btn-default"
                    });
                }
            });
            return false;
        }
    });
});

function clearform() {
    $('#form-first').each(function() {
        this.reset();
    });
}